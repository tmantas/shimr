# shimr - SHarp IMages Recursively

Compresses images and converts them to Webp format. Scans directory recursively and reflects the structure in destination directory.

Uses [sharp](https://github.com/lovell/sharp).

## Installation

Locally:
```
npm i shimr
```

Globally:
```
npm i -g shimr
```

## Usage

```
shimr -s sourcePath -d distinationPath
```

## Options

| Option    | Description                    | Default value |
|-----------|--------------------------------|---------------|
| `-s`      | Source directory               | `./src`       |
| `-d`      | Destination directory          | `./dist`      |
| `-nowebp` | Skip converting to Webp format | `false`       |


## Example

Example structure:

```
└── src
    ├── img
    │   ├── big.jpg
    │   ├── more-sizes
    │   │   └── sample@2x.png
    │   └── subfolder
    │       └── deeper-level
    │           └── sample.jpg
    └── this-is-not-an-image.txt
```

Run `shimr`. Shimr will generate new folder called `dist` reflecting the source folder's structure`:

```
└──  dist
    └── img
        ├── big.jpg
        ├── big.webp
        ├── more-sizes
        │   ├── sample.png
        │   ├── sample.webp
        │   ├── sample@2x.png
        │   └── sample@2x.webp
        └── subfolder
            └── deeper-level
                ├── sample.jpg
                └── sample.webp

```
