#!/usr/bin/env node

const shimr = require('./index')

let options = {}

process.argv.slice(2).forEach(function (val, index, array) {
  switch (val) {
    case '-s':
      options.sourceDir = array[index + 1]
      break
    case '-d':
      options.destinationDir = array[index + 1]
      break
    case '-nowebp':
      options.noWebp = true
      break
  }
})

shimr(options)
