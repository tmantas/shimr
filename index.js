'use strict'

const {resolve, extname} = require('path')
const {readdir, mkdir, access} = require('fs').promises

const sharp = require('sharp')
const logger = require('./logger')

const DEFAULT_OPTIONS = {
  sourceDir: 'src',
  destinationDir: 'dist',
  noWebp: false
}

const ALLOWED_EXTENSIONS = ['jpg', 'png', 'jpeg', 'webp']

const shimr = async function (args) {
  let options = {
    ...DEFAULT_OPTIONS,
    ...args
  }

  const SOURCE_DIR = resolve(options.sourceDir)
  const DESTINATION_DIR = resolve(options.destinationDir)

  try {
    await access(SOURCE_DIR)
  } catch (e) {
    logger.error({message: 'shimr: source directory does not exists', error: e})
    return
  }

  async function* getFiles (dir) {
    const dirEnts = await readdir(dir, {withFileTypes: true})

    for (const dirEnt of dirEnts) {
      const res = resolve(dir, dirEnt.name)

      if (dirEnt.isDirectory()) {
        yield* getFiles(res)
      } else {
        yield res
      }
    }
  }

  async function prepareDir (destinationFilePath) {
    let pathArray = destinationFilePath.split('/')
    pathArray.pop()
    const destinationDirPath = pathArray.join('/')

    await mkdir(destinationDirPath, {recursive: true})
  }

  function convertToWebp (sourceFilePath) {
    const extension = extname(sourceFilePath)
    const destinationWebpFile = sourceFilePath.replace(SOURCE_DIR, DESTINATION_DIR).replace(extension, '.webp')

    return sharp(sourceFilePath).webp().toFile(destinationWebpFile)
  }

  let hasErrorOccured = false

  for await (const sourceFilePath of getFiles(SOURCE_DIR)) {
    const extension = extname(sourceFilePath).replace('.', '').toLowerCase()

    // skipping non-image file
    if (!ALLOWED_EXTENSIONS.includes(extension)) {
      continue
    }

    // TODO: dynamic resize by '@{int}x' pattern
    const isDoubleSize = sourceFilePath.includes('@2x')

    const destinationFilePath = sourceFilePath.replace(SOURCE_DIR, DESTINATION_DIR)
    await prepareDir(destinationFilePath)

    const compressionMethod = extension.replace('jpg', 'jpeg')

    try {
      await sharp(sourceFilePath)[compressionMethod]().toFile(destinationFilePath)
    } catch (e) {
      logger.error({message: 'error compressing file', error: e, file: sourceFilePath})
      hasErrorOccured = true

      continue
    }

    if (extension !== 'webp' && !options.noWebp) {
      try {
        await convertToWebp(sourceFilePath)
      } catch (e) {
        logger.error({message: 'error converting to webp', error: e, file: sourceFilePath})
        hasErrorOccured = true

        continue
      }
    }

    if (isDoubleSize) {
      try {
        var image = sharp(sourceFilePath)
      } catch (e) {
        logger.error({message: 'error sharping image', error: e, file: sourceFilePath})
        hasErrorOccured = true

        continue
      }

      try {
        var {width} = await image.metadata()
      } catch (e) {
        logger.error({message: 'error getting image metadata', error: e, file: sourceFilePath})
        hasErrorOccured = true

        continue
      }

      // resizing image
      try {
        var resizedFile = await sharp(sourceFilePath).resize(Math.round(width / 2))
      } catch (e) {
        logger.error({message: 'error resizing image', error: e, file: sourceFilePath})
        hasErrorOccured = true

        continue
      }

      const reducedDestinationFilePath = destinationFilePath.replace('@2x', '')

      try {
        await resizedFile[compressionMethod]().toFile(reducedDestinationFilePath)
      } catch (e) {
        logger.error({message: 'error compressing resized image', error: e, file: reducedDestinationFilePath})
        hasErrorOccured = true

        continue
      }

      if (extension !== 'webp' && !options.noWebp) {
        try {
          await convertToWebp(reducedDestinationFilePath)
        } catch (e) {
          logger.error({message: 'error converting resized image to webp', error: e, file: reducedDestinationFilePath})
          hasErrorOccured = true
        }
      }
    }
  }

  if (hasErrorOccured) {
    logger.warning(`
 Done with errors
`)
  } else {
    logger.success(`
 Done!
`)
  }

}

module.exports = shimr
