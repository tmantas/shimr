const chalk = require('chalk')

const logger = {
  error: ({message, error = null, file = ''}) => {
    console.log(`
${chalk.red(message)}`)

    if (error) {
      console.log(`${chalk.red('Message:')}
  ${error.toString().replace(/[]/g, '')}`)
    }

    if (file) {
      console.log('File path: ', chalk.yellow(file))
    }
  },
  warning: (message) => {
    console.log(chalk.yellow(message))
  },
  success: (message) => {
    console.log(chalk.green(message))
  }
}

module.exports = logger
